@extends ('layout.master')

@section('content')
<div class="col-sm-8 blog-main">
    
    <hr>
    
      @include('layout.error')
    
    <form method="POST" action="/blog">
     
  <div class="form-group">
    <label for="title">Title</label>
    <input type="text" class="form-control" id="title" name="title" >
  </div>
     {{ csrf_field() }}
     
  <div class="form-group">
    <label for="body">Body</label>
    <textarea name="body" id="body" cols="60" rows="10" class="form-control"></textarea>
  </div>
     
     
     <button type="submit" id="submit" class="btn btn-primary">Publish</button>
   
</form>   
</div>

@endsection
