
@include('layout.header')

  <body>
    
@include('layout.nav')

    <div class="blog-header">
      <div class="container">
        <h1 class="blog-title">The Bootstrap Blog</h1>
        <p class="lead blog-description">An example blog template built with Bootstrap.</p>
      </div>
    </div>

    <div class="container">

      <div class="row">

       @yield('content')<!-- /.blog-main -->

     @include('layout.sidebar')<!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->

    @include('layout.footer')
