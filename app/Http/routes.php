<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/task', 'TaskController@index');

Route::get('/task/{task}', 'TaskController@show');

Route::get('/task/', 'TaskController@store');

Route::get('/blog', 'PostsController@index');

Route::get('/blog/create', 'PostsController@create');

Route::post('/blog', 'PostsController@store');

Route::get('/blog/{id}', 'PostsController@show');

//Route::get('/blog/{post}', 'PostsController@show');
